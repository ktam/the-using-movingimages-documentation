<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="generator" content="pandoc">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">
  <title></title>
  <style type="text/css">code{white-space: pre;}</style>
  <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
  <link rel="stylesheet" href="pandoc.css">
</head>
<body>
<p><a href="Contents">Contents</a></p>
<h2 id="using-the-movingimages-framework">Using the MovingImages Framework</h2>
<p>The MovingImages Framework has a fairly small programming interface.</p>
<p>MovingImages provides a context <code>MIContext</code> within which base objects are managed, the image collection is managed, and the variables dictionary is managed. There is a default context that is used if you pass in null for the MIContext.</p>
<p>Creating a new <code>MIContext</code> for different uses makes it possible to use the same JSON object commands independently of each other. For example if you are using MovingImages to render the controls in your user interface the same JSON object can be used for drawing multiple controls and each control can be varied using the variables dictionary which is unique to each <code>MIContext</code>.</p>
<p>Minimum operating system for OS X is Yosemite. Minimum operating system for iOS is iOS 8.</p>
<h3 id="logging-function">Logging function</h3>
<p>You can set a callback function for the MovingImages framework that is called every time MovingImages needs to log a message. This happens when the JSON object commands is invalid in some way. The log function is declared as a block and is given the type definition <code>MILoggingFunction</code>.</p>
<p>When the log function is called from the framework various bits of information are provided. There is a brief message describing what went wrong. There is the class name of the object, and then a string representation of the object. The object supplied is nearly always the JSON object that failed to be processed, but sometimes it is an additional piece of information in a followup call to the log function.</p>
<p>If commands and draw elements are deeply nested then as the stack unwinds from the detection of the error, the log function is repeatedly called providing ever larger amounts of content. At the break between processing &quot;drawelements&quot; and at the end of JSON object commands a line of equal signs is output indicating that this is likely the end of output for a particular error message.</p>
<p>To assign a log function you use the function.</p>
<pre><code>void MISetLogging(MILoggingFunction loggingFunction);</code></pre>
<h3 id="mimovingimageshandlecommands">MIMovingImagesHandleCommands</h3>
<p>To pass a list of commands to the MovingImages framework to be processed you use the <code>MIMovingImagesHandleCommands</code> function.</p>
<pre class="objective-c"><code>NSDictionary *MIMovingImagesHandleCommands(
                        MIContext * __nullable context,
                        NSDictionary *commands,
                        __nullable MIProgressHandler progressHandler, 
                        __nullable MICommandCompletionHandler handler)</code></pre>
<p>This function is declared in <code>MIHandleCommands.h</code></p>
<p>The MovingImages LaunchAgent uses this function as the sole entry point into MovingImages for processing JSON object commands. The LaunchAgent calls the <code>MIMovingImagesHandleCommands</code> function and passes in nil for the context, the progress handler and the completion handlers. Passing in nil for the context indicates that the default global context should be used.</p>
<p>The second parameter is the commands dictionary. The structure of the commands dictionary is same as the structure represented by the ruby generated JSON. The <a href="JSONandPropertyLists">JSON and Property Lists</a> documentation is the best place to start to learn about the structure of the commands dictionary.</p>
<p>The progress handler is called immediately before the next command is about to be processed this allows you to update the variables dictionary if you need to, or you can update a progress handler or draw an image from the image collection into your UI.</p>
<p>The completion handler is declared in MovingImages.h and is a block that takes a single argument of type BOOL which indicates whether successfully ran to completion.</p>
<p>Also declared in MovingImages.h is:</p>
<pre class="objective-c"><code>NSDictionary *MIMovingImagesHandleCommand(MIContext * __nullable context, 
                                          NSDictionary *command);</code></pre>
<p>Which performs a single command. This function is called by <code>MIMovingImagesHandleCommands</code> once for each command in the command list and is passed a dictionary containing the information to perform the command. The function returns a dictionary with a return code and a result string.</p>
<h3 id="micontext">MIContext</h3>
<p>The MIContext class manages the variables dictionary, base objects and the image collection. It has methods for adding and removing variables from the variables dictionary. You can also directly add and remove images from the image collection using methods of the MIContext object. Mostly though it would be expected that images added and removed from the image collection would be done via JSON object commands.</p>
<h3 id="drawing-views-and-controls">Drawing views and controls</h3>
<h4 id="drawing-in-os-x">Drawing in OS X</h4>
<p>The <code>MISimpleRenderer</code> class is designed to be used for drawing views and controls. Instead of taking JSON object commands the draw method of a <code>MISimpleRenderer</code> object takes a <code>drawelement</code> JSON object.</p>
<p>There is a simple example of a NSView subclass that draws to the view which is used in both the MovingImages Demo application and in the Zukini Demo application.</p>
<p><a href="https://github.com/SheffieldKevin/MovingImagesDemo/blob/master/MovingImages%20Demo/SimpleRendererView.swift">NSView subclass for drawing using MISimpleRenderer</a></p>
<p>There is also an example of a NSControl subclass that draws a numeric control and uses the variables/equations feature of MovingImages to vary the drawing dependent on the controls value.</p>
<p>The controls is implemented using IBDesignable and you can vary the control value within Interface Builder and see the control redraw.</p>
<p><a href="https://github.com/SheffieldKevin/MovingImagesDemo/blob/master/Spinner2/SpinnerController.swift">NSControl subclass for drawing using MISimpleRenderer</a></p>
<p>The code for the controller is made a little more complicated than might be necessary as it also manages a NSPopover.</p>
<h4 id="drawing-in-ios">Drawing in iOS</h4>
<p>The <a href="https://github.com/SheffieldKevin/CustomControlDemo">Custom Control Demo project</a> demonstrates using MovingImages for drawing a control in iOS. Just like for OS X drawing is done using the <code>MISimpleRenderer</code> class.</p>
<p>I've created a <a href="https://github.com/SheffieldKevin/CustomControlDemo/blob/master/CustomControlDemo/CustomControlLayer.swift">subclass of CALayer</a> which demonstrates how to use the MovingImages framework for drawing a user interface control.</p>
<h3 id="managing-resources">Managing Resources</h3>
<p>All base objects are created within a <code>MIContext</code> object context. There are three ways to dispose of base objects. The safest way is adding close object commands to the list of cleanup commands for each object you have created. After the main command list has completed with or without error the cleanup commands are run, and all cleanup commands are performed even if an earlier command returned an error.</p>
<p>Sometimes though waiting for the cleanup commands to be performed is too late. If you are processing many image files you may not want to keep all those objects open until the cleanup commands run. In that case add a close command for each object you want to close as early as you can.</p>
<p>Please see the JSON object and Property Lists documentation and specifically the documentation on SmigCommands.</p>
<p>No objects hold a strong reference to the <code>MIContext</code> object. If you have finished with any processing within a particular <code>MIContext</code> you can just dispose of that context and all the base objects it manages, any images in the image collection that it holds onto, any variable dictionaries will be disposed of at the same time. I do think it is better to manage those objects dynamically though.</p>
<p>Just like the base objects, you should also manage images in the image collection dynamically. Assigning an image to the image collection with the same identifier as an image in the collection will replace the image in the collection. You can also use the <code>removeImageWithIdentifier</code> method of the <code>MIContext</code> object to remove the image. You can also use the remove image command to remove an image and this command can be added to the main list of commands or the list of cleanup commands.</p>
<p>The variables dictionary works a little differently. Since it is possible for more than one list of commands to be processed at any time using the same <code>MIContext</code> object the actual dictionary available for accessing variables is built up from an array of dictionaries. You add another dictionary using <code>appendVariables</code> and when that variables dictionary is no longer valid you pass in the original dictionary to <code>dropVariablesDictionary</code> and it will remove that dictionary from the list of dictionaries. You can also do this dynamically, i.e. while a list of commands is being processed and in that case you use the <code>dropVariablesDictionary(dropDict, appendNewDictionary:newDict)</code> method which makes sure that the combined variables dictionary is fully defined at any time it is accessed.</p>
<p><a href="Contents">Contents</a></p>
<p>© Zukini Ltd. 2015</p>
</body>
</html>
