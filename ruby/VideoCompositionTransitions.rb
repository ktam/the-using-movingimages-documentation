#!/usr/bin/env ruby
require 'moving_images'

# This script will pull in two movies and create two video tracks. The first
# transition will be from video track 0 to video track 1, and the second
# transition will be back to video track 0. First transition will be a simple
# dissolve ramp transition. The second transition will be transform ramp where
# video track 1 is gradually replaced by video track 0. 2 seconds.

include MovingImages
include MICGDrawing
include CommandModule
include MIMovie

def make_videocomposition()
  variables = {
    movie1path: "~/Movies/IMG_0532.MOV",
    movie2path: "~/Movies/IMG_0536.MOV",
    exportfilepath: "~/Desktop/videocompositionmovie.mov",
    compositionmapimage: "~/Desktop/videocompositiondiagram.jpg"
  }
  theCommands = SmigCommands.new
  theCommands.variables = variables
  begin
    movieImporter1 = theCommands.make_createmovieimporter("~/DummyMovie.mov",
                                      pathsubstitutionkey: :movie1path)

    movieImporter2 = theCommands.make_createmovieimporter("~/DummyMovie.mov",
                                      pathsubstitutionkey: :movie2path)

    # Create the movie editor where the video composition will happen.
    movieEditorObject = theCommands.make_createmovieeditor()

    addVideoTrackCommand = CommandModule.make_createtrackcommand(
                                                movieEditorObject,
                                     mediatype: :vide)

    # Create two video tracks using the addVideoTrackCommand.
    theCommands.add_command(addVideoTrackCommand)
    theCommands.add_command(addVideoTrackCommand)

    bitmapSize = MIShapes.make_size(800, 600)
    # Need the bitmap to render the video composition diagram to.
    bitmap = theCommands.make_createbitmapcontext(
                              size: bitmapSize,
                            preset: :PlatformDefaultBitmapContext,
                           profile: :kCGColorSpaceGenericRGB)

    finalImageID = SecureRandom.uuid

    assignImageToCollection = CommandModule.make_assignimage_tocollection(
                                                    bitmap,
                                        identifier: finalImageID)

    track0 = MovieTrackIdentifier.make_movietrackid_from_mediatype(
                                        mediatype: :vide,
                                       trackindex: 0)

    track1 = MovieTrackIdentifier.make_movietrackid_from_mediatype(
                                        mediatype: :vide,
                                       trackindex: 1)

    timeZero = MovieTime.make_movietime(timevalue: 0, timescale: 1)
    inputDuration0 = MovieTime.make_movietime(timevalue: 4, timescale: 1)
    timeRange0 = MovieTime.make_movie_timerange(start: timeZero,
                                            duration: inputDuration0)
    inputDuration1 = MovieTime.make_movietime(timevalue: 6, timescale: 1)
    twoSecsIn = MovieTime.make_movietime(timevalue: 2, timescale: 1)
    timeRange1 = MovieTime.make_movie_timerange(start: twoSecsIn,
                                            duration: inputDuration1)
    sixSecsIn = MovieTime.make_movietime(timevalue: 6, timescale: 1)
    timeRange2 = MovieTime.make_movie_timerange(start: sixSecsIn,
                                            duration: inputDuration0)
    instructionDuration = MovieTime.make_movietime(timevalue:2, timescale:1)
    passThruDuration = instructionDuration
    firstTansitionStartTime = MovieTime.make_movietime(timevalue:2, timescale:1)
    firstTransitionDuration = instructionDuration
    secondPassThruStartTime = MovieTime.make_movietime(timevalue:4, timescale:1)
    secondTransitionStartTime = MovieTime.make_movietime(timevalue:6, timescale:1)
    secondTransitionDuration = instructionDuration
    thirdPassThruStartTime = MovieTime.make_movietime(timevalue:8, timescale:1)
    thirdPassThruDuration = instructionDuration

    # insert first track segment into track 0 for 4 seconds
    insertTrackSegmentCommand1 = CommandModule.make_inserttracksegment(
                            movieEditorObject, 
                     track: track0,
             source_object: movieImporter1,
              source_track: track0,
             insertiontime: MovieTime.make_movietime(timevalue:0, timescale:1), 
          source_timerange: timeRange0)
    theCommands.add_command(insertTrackSegmentCommand1)
    
    # insert second track segment into track 1 for 6 seconds starting 2 secs.
    insertTrackSegmentCommand2 = CommandModule.make_inserttracksegment(
                            movieEditorObject, 
                     track: track1,
             source_object: movieImporter2,
              source_track: track0,
             insertiontime: MovieTime.make_movietime(timevalue:2, timescale:1), 
          source_timerange: timeRange1)
    theCommands.add_command(insertTrackSegmentCommand2)

    # insert third track segment into track 0 starting at 6 seconds lasting 4.
    insertTrackSegmentCommand3 = CommandModule.make_inserttracksegment(
                            movieEditorObject, 
                     track: track0,
             source_object: movieImporter1,
              source_track: track0,
             insertiontime: MovieTime.make_movietime(timevalue:6, timescale:1), 
          source_timerange: timeRange2)
    theCommands.add_command(insertTrackSegmentCommand3)

    # Now create a pass thru instruction
    passThru1 = VideoLayerInstructions.new
    passThru1.add_passthrulayerinstruction(track: track0)
    passThru1TimeRange = MovieTime.make_movie_timerange(start: timeZero,
                                                    duration: passThruDuration)
    passThru1Command = CommandModule.make_addvideoinstruction(movieEditorObject,
                                                 timerange: passThru1TimeRange,
                                         layerinstructions: passThru1)
    theCommands.add_command(passThru1Command)

    dissolveTimeRange = MovieTime.make_movie_timerange(
                                               start: firstTansitionStartTime,
                                            duration: firstTransitionDuration)

    # Now create a dissolve ramp layer instruction.
    dissolveRamp = VideoLayerInstructions.new
    dissolveRamp.add_opacityramplayerinstruction(track: track0, 
                                     startopacityvalue: 1.0,
                                       endopacityvalue: 0.0,
                                             timerange: dissolveTimeRange)
    dissolveRamp.add_passthrulayerinstruction(track: track1)
    dissolveRampCommand = CommandModule.make_addvideoinstruction(
                                                      movieEditorObject,
                                           timerange: dissolveTimeRange,
                                   layerinstructions: dissolveRamp)
    theCommands.add_command(dissolveRampCommand)
    
    # Now create a pass thru instruction
    passThru2 = VideoLayerInstructions.new
    passThru2.add_passthrulayerinstruction(track: track1)
    passThru2TimeRange = MovieTime.make_movie_timerange(
                                              start: secondPassThruStartTime,
                                           duration: passThruDuration)
    passThru2Command = CommandModule.make_addvideoinstruction(
                                                       movieEditorObject,
                                            timerange: passThru2TimeRange,
                                    layerinstructions: passThru2)
    theCommands.add_command(passThru2Command)

    # Now add a transform ramp layer instruction.
    transformRampTimeRange = MovieTime.make_movie_timerange(
                                              start: secondTransitionStartTime,
                                           duration: secondTransitionDuration)
    
    startTransform = MITransformations.make_affinetransform()
    endTransform = MITransformations.make_contexttransformation()
    scaleXY = MIShapes.make_point(0.0, 1.0)
    MITransformations.add_scaletransform(endTransform, scaleXY)
    
    transformRamp = VideoLayerInstructions.new
    transformRamp.add_transformramplayerinstruction(track: track1,
                                starttransformvalue: startTransform,
                                  endtransformvalue: endTransform,
                                          timerange: transformRampTimeRange)
    transformRamp.add_passthrulayerinstruction(track: track0)
    transformRampCommand = CommandModule.make_addvideoinstruction(
                                                     movieEditorObject,
                                          timerange: transformRampTimeRange,
                                  layerinstructions: transformRamp)
    theCommands.add_command(transformRampCommand)

    # Now create a pass thru instruction
    passThru3 = VideoLayerInstructions.new
    passThru3.add_passthrulayerinstruction(track: track0)
    passThru3TimeRange = MovieTime.make_movie_timerange(
                                                start: thirdPassThruStartTime,
                                             duration: thirdPassThruDuration)
    passThru3Command = CommandModule.make_addvideoinstruction(
                                                       movieEditorObject,
                                            timerange: passThru3TimeRange,
                                    layerinstructions: passThru3)
    theCommands.add_command(passThru3Command)

    # Lets create a composition map image so we can save it.
    imageIdentifier = SecureRandom.uuid
    addCompositionImage = CommandModule.make_assignimage_tocollection(
                                                movieEditorObject,
                                    identifier: imageIdentifier)
    theCommands.add_command(addCompositionImage)
    imageExporter = theCommands.make_createexporter("Dummypath.jpg",
                               pathsubstitutionkey: :compositionmapimage)
    addImageToExporter = CommandModule.make_addimage_fromimagecollection(
                                   imageExporter,
                  imageidentifier: imageIdentifier)
    theCommands.add_command(addImageToExporter)
    exportCommand = CommandModule.make_export(imageExporter)
    theCommands.add_command(exportCommand)


    # Now lets export the movie. This command may take some time so it is added
    # to process commands.
    exportMovieCommand = CommandModule.make_movieeditor_export(
                                              movieEditorObject,
                                exportpreset: :AVAssetExportPreset1280x720,
                              exportfilepath: "DummyPath.mov",
                              exportfiletype: :'com.apple.quicktime-movie',
                         pathsubstitutionkey: :exportfilepath)
    theCommands.add_command(exportMovieCommand)
    # Add a remove image from image collection command to cleanup.
    theCommands.add_tocleanupcommands_removeimagefromcollection(
                                                          imageIdentifier)
    Smig.perform_commands(theCommands)
  rescue RuntimeError => e
    unless Smig.exitvalue.zero?
      puts "Exit string: " + Smig.exitstring
      puts " Exit status: " + Smig.exitvalue.to_s
    end
    puts e.message
    puts e.backtrace.to_s
  end
  puts JSON.pretty_generate(theCommands.commandshash)
end

make_videocomposition()
